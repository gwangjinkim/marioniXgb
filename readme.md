# Installation

```
pip install -e 'git+https://gwangjinkim@bitbucket.org/gwangjinkim/marionixgb.git#egg=marionixgb'
```

# Usage

```
from marionixgb import Marioni, XGBoostMarioni
```

# 

# reverse complement a sequence
oh.rev_compl("AAGTCGTN")

# inspect data for translation
oh.encoder
oh.decoder
oh.universe
oh.syonyms

```


```
# document the version
Linux:
$ pip freeze | grep seq2one_hot


Windows:
c:\> pip freeze | findstr seq2one_hot


# pip install sinfo
# sinfo.sinfo()
# # this gives only NA for locally installed packages
```
