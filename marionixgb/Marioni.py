
"""
What I want to have is:

calculation of all celltype-dependent correlation of all DE genes
and showing which of the genes are very likely directly regulated by Eo or Bra.

For that, I want also to group certain celltypes and use their correlation
values - and sort the genes.

Then, I want to do xgboost on all DE genes (and hox genes) and show,
which of the genes are most likely directly regulated by Eo/Bra.
(in which in Top20 are Eo or Bra?)

Then, if I can manage it, I want to answer, where around Hoxb1 and some of the DE genes are probably Eo/Bra binding sites?
Which known enhancers are around them?
"""

# ss
# conda activate sc_imput
# ipython

import xgboost as xg
from sklearn.metrics import mean_squared_error as MSE

import paths
import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mp

import math

import scanpy as sc
import scanpy.external as sce

import npckl

import scipy

import functools

from ttsplit import ttsplit as tts

import seaborn as sns

import xlsx2dfs

class Marioni:
    
    def __init__(self):

        # determine the paths
        self.data_dir = os.path.join(paths.p_chipseq_.base_dir, 
                                      "data", 
                                      "10x_Data_Sebastian_Harmony_Integration")
        self.anno_fpath = os.path.join(self.data_dir, "cell_type_annotation_josephus.csv")
        self.anno_df =pd.read_csv(self.anno_fpath, sep="\t")
        self.celltype2color = {"Epiblast": "#635547",
                     "Primitive Streak": "#DABE99",
                     "Caudal epiblast": "#9e6762",

                     "PGC": "#FACB12",

                     "Anterior Primitive Streak": "#c19f70",
                     "Notochord": "#0F4A9C",
                     "Def. endoderm": "#F397C0",
                     "Gut": "#EF5A9D",

                     "Nascent mesoderm": "#C594BF",
                     "Mixed mesoderm": "#DFCDE4",
                     "Intermediate mesoderm": "#139992",
                     "Caudal Mesoderm": "#3F84AA",
                     "Paraxial mesoderm": "#8DB5CE",
                     "Somitic mesoderm": "#005579",
                     "Pharyngeal mesoderm": "#C9EBFB",
                     "Cardiomyocytes": "#B51D8D",
                     "Allantois": "#532C8A",
                     "ExE mesoderm": "#8870ad",
                     "Mesenchyme": "#cc7818",

                     "Haematoendothelial progenitors": "#FBBE92",
                     "Endothelium": "#ff891c",
                     "Blood progenitors 1": "#f9decf",
                     "Blood progenitors 2": "#c9a997",
                     "Erythroid1": "#C72228",
                     "Erythroid2": "#f79083",
                     "Erythroid3": "#EF4E22",

                     "NMP": "#8EC792",

                     "Rostral neurectoderm": "#65A83E",
                     "Caudal neurectoderm": "#354E23",
                     "Neural crest": "#C3C388",
                     "Forebrain/Midbrain/Hindbrain": "#647a4f",
                     "Spinal cord": "#CDE088",

                     "Surface ectoderm": "#f7f79e",

                     "Visceral endoderm": "#F6BFCB",
                     "ExE endoderm": "#7F6874",
                     "ExE ectoderm": "#989898",
                     "Parietal endoderm": "#1A1A1A",

                     "Other": "lightgrey"
        }
        self.cell2celltype = self.read_cell2celltype(self.anno_fpath)
        self.cell2color = {key: self.celltype2color[self.cell2celltype[key]] for key in self.cell2celltype}
        
        self.cell2color ={key: self.celltype2color[self.cell2celltype[key]] for key in self.cell2celltype.keys()}
        self.npy_paths = glob.glob(os.path.join(self.data_dir, "gastrulation_harmony_integration_*.npy"))
        self.time2npy_path = {os.path.basename(fp).replace("gastrulation_harmony_integration_", "").replace(".npy", ""):
                              fp for fp in self.npy_paths}
        self.time2timepoint = {key: val for key, val in zip(
          ["e6_50", "e6_75", "e7_00", "e7_25", "e7_50", "e7_75", "e8_00", "e8_25", "e8_50"],
          ["E6.5", "E6.75", "E7.0", "E7.25", "E7.5", "E7.75", "E8.0", "E8.25", "E8.5"])}
        self.time2counts = {}
        self.time2anno = {}
        
        self.endodermal_celltypes = ['Def. endoderm', 'ExE endoderm', 'Visceral endoderm', 'Parietal endoderm']
        self.mesodermal_celltypes = ['Anterior Primitive Streak', 'Mixed mesoderm', \
                        'Nascent mesoderm', 'Intermediate mesoderm', 'PGC', \
                        'Caudal Mesoderm', 'ExE mesoderm', 'Mesenchyme', \
                        'Paraxial mesoderm', 'Pharyngeal mesoderm', \
                        'Primitive Streak', 'Somitic mesoderm']

    def read_cell2celltype(self, fpath, na_fill="Other"):
        df = pd.read_csv(fpath, index_col=None)
        df = df.fillna(na_fill)
        return {row[0]: row[3] for i, row in df.iterrows()}
    
    def get_counts(self, timepoint, imputed=False, k=5):
        if "E" not in timepoint:
            timepoint = self.time2timepoint[timepoint]
        key_tuple = (timepoint, imputed, k)
        fpath = self.time2npy_path[timepoint]
        if key_tuple not in self.time2counts:
            if imputed:
                self.time2counts[key_tuple] = self.read_and_impute(fpath, k=k)
            else:
                self.time2counts[key_tuple] = self.read(fpath)
        return self.time2counts[key_tuple]
    
    def get_labels(self, timepoint):
        df = self.get_counts(timepoint)
        return [self.cell2celltype[x] for x in df.index]
    
    def read(self, fpath):
        df = npckl.load_npckl(fpath).transpose()
        df = df.fillna(0) # E8.25 had some na
        return df
    
    def read_and_impute(self, fpath, k=5):
        df = self.read(fpath)  
        adata = sc.AnnData(df)
        sc.pp.log1p(adata)
        sce.pp.magic(adata, name_list='all_genes', knn=k)
        dfi = adata.to_df()
        return dfi
    
    def plot_genes(self, 
                    timepoint, 
                    x_gene, 
                    y_gene, 
                    imputed=True, 
                    k=5, 
                    alpha=0.1,
                    out=None,
                    seed=42,
                    with_legend=True,
                    figsize=(10, 10)):
        df = self.get_counts(timepoint, imputed=imputed, k=k)
        np.random.seed(seed)
        plt.clf()
        fig, ax = plt.subplots(figsize=figsize)
        celltypes = [self.cell2celltype[x] for x in df.index] # self.get_labels(timepoints)
        colors = [self.cell2color[x] for x in df.index]
        ax.scatter(x=df[x_gene],
                   y=df[y_gene],
                   alpha=alpha,
                   label=celltypes,
                   c=colors,
                   edgecolors='none',
                   cmap=self.cell2color)
        plt.xlabel(f"{x_gene} normed expression")
        plt.ylabel(f"{y_gene} normed expression")
        if with_legend:
            labs = sorted(list(set([self.cell2celltype[x] for x in df.index])))
            cols = [self.celltype2color[x] for x in labs]
            recs = [mp.Rectangle((0,0),1,1,fc=c, alpha=alpha) for c in cols]
            plt.legend(recs, labs, loc="best", title="Celltypes", frameon=False)
        if out is not None:
            plt.savefig(out)
        else:
            plt.show()
    
    def correlation_by_celltype(self,
                                timepoint,
                                x_gene,
                                y_gene,
                                imputed=True,
                                k=5,
                                seed=42,
                                corr_method="spearman"): 
        corr_func= {"pearson": scipy.stats.pearsonr, 
                    "spearman": scipy.stats.spearmanr}[corr_method]
        df = self.get_counts(timepoint, imputed=imputed, k=k)
        np.random.seed(seed)
        celltypes = [self.cell2celltype[x] for x in df.index]
        x, y = df[x_gene], df[y_gene]
        # unique_celltypes = sorted(list(set(celltypes)))
        grouped = {}
        for x_, y_, z_ in zip(x, y, celltypes):
            res = grouped.get(z_, [[], []])
            res[0] = res[0] + [x_]
            res[1] = res[1] + [y_]
            grouped[z_] = res
        corr_df = pd.DataFrame({key: corr_func(grouped[key][0], grouped[key][1])  for key in grouped})
        corr_df.index = ["r", "p_val"]
        return corr_df, grouped
    
    def correlations(self,
                     timepoint,
                     x_gene,
                     y_genes,
                     imputed=True,
                     k=5,
                     seed=42,
                     corr_method="spearman"):
        # unique_celltypes = sorted(list(set(celltypes)))
        df = self.get_counts(timepoint, imputed=imputed, k=k)
        y_genes = [x for x in y_genes if x in [x for x in df.columns]] # filter out non-existent genes!
        
        res_df = []
        res_grouped = {}
        for y_gene in y_genes:
            corr_df, grouped = self.correlation_by_celltype(timepoint=timepoint,
                                                            x_gene=x_gene,
                                                            y_gene=y_gene,
                                                            imputed=imputed,
                                                            k=k,
                                                            seed=seed,
                                                            corr_method=corr_method)
            res_df.append(corr_df)
            res_grouped[y_gene] = grouped
        corr_df = functools.reduce(lambda df1, df2: pd.concat([df1, df2], axis=0), [df.iloc[0:1, :] for df in res_df])
        corr_df_sd = functools.reduce(lambda df1, df2: pd.concat([df1, df2], axis=0), [df.iloc[1:2, :] for df in res_df])
        corr_df.index = y_genes
        corr_df_sd.index = y_genes
        return corr_df, corr_df_sd, res_grouped
    
    def correlation_by_celltype_fusing(self,
                                timepoint,
                                x_gene,
                                y_gene,
                                imputed=True,
                                k=5,
                                seed=42,
                                corr_method="spearman",
                                fuses={}): 
        corr_func= {"pearson": scipy.stats.pearsonr, 
                    "spearman": scipy.stats.spearmanr}[corr_method]
        corr_df, grouped = self.correlation_by_celltype(
                                timepoint=timepoint,
                                x_gene=x_gene,
                                y_gene=y_gene,
                                imputed=imputed,
                                k=k,
                                seed=seed,
                                corr_method=corr_method)
        res_grouped = {key: np.concatenate([np.array(grouped.get(ct, [[], []])) \
                                            for ct in fuses[key]], 
                                           axis=1) \
                            for key in fuses}
        res_corr_df = pd.DataFrame({key: corr_func(res_grouped[key][0], res_grouped[key][1]) \
                                   for key in res_grouped})
        res_corr_df.index = ["r", "p_val"]
        return res_corr_df, res_grouped
    
    def correlations_fusing(self,
                     timepoint,
                     x_gene,
                     y_genes,
                     imputed=True,
                     k=5,
                     seed=42,
                     corr_method="spearman",
                     fuses={}):
        # unique_celltypes = sorted(list(set(celltypes)))
        df_genes = self.get_counts(timepoint, imputed=imputed, k=k).columns
        y_genes = [x for x in y_genes if x in [x for x in df_genes]] 
        # filter out non-existent genes!
        
        res_df = []
        res_grouped = {}
        for y_gene in y_genes:
            corr_df, grouped = self.correlation_by_celltype_fusing(
                                        timepoint=timepoint,
                                        x_gene=x_gene,
                                        y_gene=y_gene,
                                        imputed=imputed,
                                        k=k,
                                        seed=seed,
                                        corr_method=corr_method,
                                        fuses=fuses)
            res_df.append(corr_df)
            res_grouped[y_gene] = grouped
        corr_df = functools.reduce(lambda df1, df2: pd.concat([df1, df2], axis=0), [df.iloc[0:1, :] for df in res_df])
        corr_df_sd = functools.reduce(lambda df1, df2: pd.concat([df1, df2], axis=0), [df.iloc[1:2, :] for df in res_df])
        corr_df.index = y_genes
        corr_df_sd.index = y_genes
        return corr_df, corr_df_sd, res_grouped
    
    def get_celltypes(self):
        return sorted(list(set(self.cell2celltype.values())))

